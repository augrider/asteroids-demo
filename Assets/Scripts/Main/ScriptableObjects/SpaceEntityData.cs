using System;
using System.Collections;
using System.Collections.Generic;
using SpaceEntitySystem;
using UnityEngine;

namespace GameScriptableObjects
{
    public abstract class SpaceEntityData : ScriptableObject
    {
        public int scoreCost;
        public float startSpeed;
        public virtual bool isPlayer => false;

        [SerializeField] private GameObject prefab;


        public virtual GameObject SpawnEntity(Transform parent, Vector3 position)
        {
            var newGameObject = GameObject.Instantiate(prefab, parent, false);
            newGameObject.transform.SetPositionAndRotation(position, Quaternion.identity);

            return newGameObject;
        }

        public virtual IEntityInteractions GetEntityInteractions(SpaceEntity entity) => new BaseEntityInteractions(entity);


        public abstract void OnSpawned(SpaceEntity entity);
    }
}