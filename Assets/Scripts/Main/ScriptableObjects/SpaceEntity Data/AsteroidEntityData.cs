using System.Collections;
using System.Collections.Generic;
using CommonExtentions;
using SpaceEntitySystem;
using UnityEngine;

namespace GameScriptableObjects
{
    [CreateAssetMenu(menuName = "Game Scriptable Objects/Entities/Asteroid")]
    public class AsteroidEntityData : SpaceEntityData
    {
        [SerializeField] private float maxRotationSpeed = 15;

        [SerializeField] private SpaceEntityData pieceData;
        [SerializeField] private int piecesCount = 4;


        public override IEntityInteractions GetEntityInteractions(SpaceEntity entity) => new SpawnOnDeath(entity, pieceData, piecesCount);


        public override void OnSpawned(SpaceEntity entity)
        {
            entity.physics.SetRotationSpeed(Random.Range(-maxRotationSpeed, maxRotationSpeed));
        }
    }
}