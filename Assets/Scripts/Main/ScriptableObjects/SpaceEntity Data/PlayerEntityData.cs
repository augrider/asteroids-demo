using System;
using System.Collections;
using System.Collections.Generic;
using SpaceEntitySystem;
using UnityEngine;
using WeaponSystem;

namespace GameScriptableObjects
{
    [CreateAssetMenu(menuName = "Game Scriptable Objects/Entities/Player")]
    public class PlayerEntityData : SpaceEntityData, IObjectPhysicsData
    {
        public override bool isPlayer => true;

        [SerializeField] private PlayerControls controls;

        public float dragValue => _dragValue;
        [SerializeField] private float _dragValue;

        public float dragSpeedMultiplier => _dragSpeedMultiplier;
        [SerializeField] private float _dragSpeedMultiplier;

        [SerializeField] private float maxAcceleration;

        [SerializeField] private float rotationSpeed = 15;

        [SerializeField] private WeaponData bullet;
        [SerializeField] private WeaponData laser;


        public override void OnSpawned(SpaceEntity entity)
        {
            entity.entityLauncher.Initialize(entity, bullet, laser);
            entity.physics.SetPhysicsParameters(this);

            Debug.LogFormat("Player spawned!");
        }

        public override IEntityInteractions GetEntityInteractions(SpaceEntity entity)
        {
            var interaction = new PlayerInteractions(entity, controls, maxAcceleration, rotationSpeed);
            interaction.SetWeapons(bullet, laser);

            return interaction;
        }
    }
}