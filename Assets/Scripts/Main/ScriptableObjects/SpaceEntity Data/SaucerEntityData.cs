using System;
using System.Collections;
using System.Collections.Generic;
using SpaceEntitySystem;
using UnityEngine;

namespace GameScriptableObjects
{
    [CreateAssetMenu(menuName = "Game Scriptable Objects/Entities/Saucer")]
    public class SaucerEntityData : SpaceEntityData
    {
        [SerializeField] private PlayerState playerState;
        [SerializeField] private float speedComponentMultiplier = 0.5f;


        public override void OnSpawned(SpaceEntity entity) { }

        public override IEntityInteractions GetEntityInteractions(SpaceEntity entity) => new Chaser(entity, playerState, startSpeed, speedComponentMultiplier);
    }
}