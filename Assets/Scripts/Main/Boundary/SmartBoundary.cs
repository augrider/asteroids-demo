using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceEntitySystem
{
    public class SmartBoundary : MonoBehaviour
    {
        [SerializeField] private bool invertHorizontal;
        [SerializeField] private bool invertVertical;

        private Dictionary<Collider2D, IBoundaryHandler> entitiesInside = new Dictionary<Collider2D, IBoundaryHandler>();


        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.gameObject.TryGetComponent<SpaceEntity>(out var entity))
                return;

            entitiesInside.Add(other, entity);
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            if (entitiesInside.ContainsKey(other) && entitiesInside[other].AllowedToInvert(invertHorizontal, invertVertical))
                InvertPosition(other.transform);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            entitiesInside.Remove(other);
        }



        public void InvertPosition(Transform other)
        {
            var invertX = invertHorizontal ? -1 : 1;
            var invertY = invertVertical ? -1 : 1;

            other.position = new Vector2(other.transform.position.x * invertX, other.transform.position.y * invertY);
        }
    }
}