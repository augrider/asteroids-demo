using System.Collections;
using System.Collections.Generic;
using GameScriptableObjects;
using GameScriptableObjects.Events;
using UnityEngine;

namespace SpaceEntitySystem
{
    public class PlayerStateUpdate : MonoBehaviour
    {
        [SerializeField] private SpaceEntity entity;

        [SerializeField] private PlayerState state;
        [SerializeField] private RemoveEntityEvent removeEntityEvent;


        void OnEnable()
        {
            removeEntityEvent.Subscribe(OnEntityDeath);
        }

        void OnDisable()
        {
            removeEntityEvent.Unsubscribe(OnEntityDeath);
        }


        void Update()
        {
            state.UpdateState(entity);
        }



        private void OnEntityDeath(SpaceEntity entity)
        {
            if (this.entity != entity)
                return;

            state.ToggleIsAlive(false);
        }
    }
}