using UnityEngine;

namespace SpaceEntitySystem
{
    /// <summary>
    /// Class for handling entity physics
    /// </summary>
    [System.Serializable]
    public class SpacePhysics
    {
        private IPositionAdapter positionAdapter;
        private IRotationAdapter rotationAdapter;
        private IObjectPhysicsData physicsData;

        public Vector2 appliedAcceleration { get; private set; }
        public Vector2 speedVector { get; private set; }
        public float rotationSpeed { get; private set; }

        public float speed => speedVector.magnitude;


        public SpacePhysics(IPositionAdapter positionAdapter, IRotationAdapter rotationAdapter)
        {
            this.positionAdapter = positionAdapter;
            this.rotationAdapter = rotationAdapter;
        }

        public void SetPhysicsParameters(IObjectPhysicsData physicsData) => this.physicsData = physicsData;


        public void OnUpdate(float deltaTime)
        {
            ApplyRotationSpeed(deltaTime, rotationSpeed);

            ApplyAcceleration(deltaTime, appliedAcceleration - GetDragValue(speedVector));
            ApplySpeed(deltaTime, speedVector);
        }


        public void SetSpeed(Vector2 speedVector) => this.speedVector = speedVector;
        public void SetAcceleration(Vector2 acceleration) => this.appliedAcceleration = acceleration;

        public void SetRotationSpeed(float rotationSpeed) => this.rotationSpeed = rotationSpeed;
        public void SetDirection(Vector2 direction) => rotationAdapter.SetDirection(direction.normalized);



        private void ApplyRotationSpeed(float deltaTime, float rotationSpeed)
        {
            rotationAdapter.angle -= rotationSpeed * deltaTime;
        }

        private void ApplySpeed(float deltaTime, Vector2 speed)
        {
            positionAdapter.position += speed * deltaTime;
        }

        private void ApplyAcceleration(float deltaTime, Vector2 acceleration)
        {
            speedVector += acceleration * deltaTime;
        }


        private Vector2 GetDragValue(Vector2 speedVector)
        {
            if (physicsData == null)
                return Vector2.zero;

            return (physicsData.dragValue + physicsData.dragSpeedMultiplier * speedVector.magnitude) * speedVector.normalized;
        }
    }
}