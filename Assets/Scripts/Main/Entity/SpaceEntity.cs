using System;
using System.Collections;
using System.Collections.Generic;
using GameScriptableObjects;
using UnityEngine;
using WeaponSystem;

namespace SpaceEntitySystem
{
    /// <summary>
    /// Base class for all game entities
    /// </summary>
    public class SpaceEntity : MonoBehaviour, IPositionAdapter, IRotationAdapter, IBoundaryHandler
    {
        public SpacePhysics physics { get; private set; }
        public SpaceInteractions entityInteractions => interactions;

        public Launcher entityLauncher => launcher;
        public SpaceEntityData entityData => _entityData;


        public Vector2 position { get => transform.position; set => transform.position = value; }

        public float angle { get => rotationTransform.eulerAngles.z; set => rotationTransform.Rotate(0, 0, value - angle); }
        public Vector2 forward => rotationTransform.up;

        [SerializeField] private Transform rotationTransform;
        [SerializeField] private Renderer entityRender;

        [SerializeField] private SpaceInteractions interactions;

        [SerializeField] private Launcher launcher;

        [SerializeField] private SpaceEntityData _entityData;


        //Initialize this if entity data was assigned in inspector
        void Awake()
        {
            InitComponents();

            if (_entityData)
                Initialize(_entityData);
        }

        public void Initialize(SpaceEntityData entityData)
        {
            this._entityData = entityData;

            interactions.Initialize(this, entityData.GetEntityInteractions(this));
            entityData.OnSpawned(this);
        }


        void FixedUpdate()
        {
            physics.OnUpdate(Time.fixedDeltaTime);
        }


        public void SetDirection(Vector2 value) => this.rotationTransform.up = value;

        public bool AllowedToInvert(bool invertHorizontal, bool invertVertical)
        {
            if (entityRender.isVisible)
                return false;

            if (invertHorizontal)
                return Mathf.Sign(position.x * physics.speedVector.x) > 0;

            if (invertVertical)
                return Mathf.Sign(position.y * physics.speedVector.y) > 0;

            return false;
        }



        private void InitComponents()
        {
            this.physics = new SpacePhysics(this, this);
        }
    }
}