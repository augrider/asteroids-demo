using System.Collections;
using System.Collections.Generic;
using GameScriptableObjects;
using UnityEngine;

namespace SpaceEntitySystem
{
    public class SpaceInteractions : MonoBehaviour
    {
        private SpaceEntity entity;
        private IEntityInteractions interactions;

        [SerializeField] private GameScriptableObjects.Events.CreateEntityEvent createEntityEvent;
        [SerializeField] private GameScriptableObjects.Events.RemoveEntityEvent removeEntityEvent;

        [SerializeField] private float spawnControlCooldown = 1;
        [SerializeField] private bool allowControl = true;


        public void Initialize(SpaceEntity entity, IEntityInteractions interactions)
        {
            this.entity = entity;
            this.interactions = interactions;
        }


        IEnumerator Start()
        {
            yield return null;
            yield return null;

            yield return new WaitForSeconds(spawnControlCooldown);

            ToggleAllowControl(true);
        }

        // Update is called once per frame
        void Update()
        {
            interactions?.OnUpdate(allowControl);
        }

        /// <summary>
        /// Function for processing on hit by other ship or boundary
        /// </summary>
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.TryGetComponent<SpaceEntity>(out var entity))
            {
                if (interactions.OnCollision(entity))
                    OnHit();
                return;
            }
        }

        public void OnHit()
        {
            Debug.LogWarning("Hit!");
            interactions.OnHit();

            RemoveFromGame();
        }


        public void ToggleAllowControl(bool value) => this.allowControl = value;

        public void AddToGame(SpaceEntityData entityData, Vector3 position, Vector2 speed) => createEntityEvent.Raise(entityData, position, speed);
        public void RemoveFromGame() => removeEntityEvent.Raise(entity);
    }
}