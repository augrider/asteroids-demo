using System.Collections;
using System.Collections.Generic;
using GameScriptableObjects;
using GameScriptableObjects.Events;
using UnityEngine;

namespace SpaceEntitySystem.Spawn
{
    public class EdgesSpawnHandler : ISpawnHandler
    {
        private Bounds bounds;


        public EdgesSpawnHandler(ManagerSystem.GameArea gameArea)
        {
            this.bounds = new Bounds(gameArea.floor.position, gameArea.floor.rect.size);
        }


        public void SpawnEntities(CreateEntityEvent createEntityEvent, params SpaceEntityData[] entities)
        {
            foreach (SpaceEntityData entityData in entities)
            {
                var spawnPoint = GetRandomPoint();
                createEntityEvent.Raise(entityData, spawnPoint, GetRandomDirection(spawnPoint) * entityData.startSpeed);
            }
        }



        private Vector3 GetRandomPoint()
        {
            var circlePoint = Random.insideUnitCircle.normalized * bounds.size.magnitude;
            return bounds.ClosestPoint(circlePoint);
        }

        private Vector3 GetRandomDirection(Vector3 spawnPoint)
        {
            var circlePoint = (Vector3)Random.insideUnitCircle.normalized * bounds.extents.magnitude * 0.1f;
            return (circlePoint - spawnPoint).normalized;
        }
    }
}