using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceEntitySystem
{
    public class Chaser : BaseEntityInteractions
    {
        [SerializeField] private GameScriptableObjects.PlayerState playerState;

        private float speedAbsolute;
        private float speedMultiplier;


        public Chaser(SpaceEntity spaceEntity, GameScriptableObjects.PlayerState playerState, float speedAbsolute, float speedMultiplier) : base(spaceEntity)
        {
            this.playerState = playerState;

            this.speedAbsolute = speedAbsolute;
            this.speedMultiplier = speedMultiplier;
        }


        public override void OnUpdate(bool allowControl)
        {
            if (allowControl)
                AccelerateTowardsPlayer(playerState.position.runtimeValue);
        }



        private void AccelerateTowardsPlayer(Vector2 playerPosition)
        {
            var vector = (playerPosition - entity.position);
            entity.physics.SetSpeed((speedAbsolute + speedMultiplier * vector.magnitude) * vector.normalized);
        }
    }
}