using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceEntitySystem
{
    public class BaseEntityInteractions : IEntityInteractions
    {
        protected SpaceEntity entity { get; private set; }


        public BaseEntityInteractions(SpaceEntity spaceEntity)
        {
            this.entity = spaceEntity;
        }


        public virtual void OnUpdate(bool allowControl) { }

        public virtual void OnHit() { }

        public virtual bool OnCollision(SpaceEntity other)
        {
            if (entity.entityData.isPlayer == other.entityData.isPlayer)
                return false;

            Debug.LogWarningFormat("{0}: collision detected!", entity);

            return true;
        }
    }
}