using System.Collections;
using System.Collections.Generic;
using CommonExtentions;
using GameScriptableObjects;
using UnityEngine;


namespace SpaceEntitySystem
{
    public class SpawnOnDeath : BaseEntityInteractions
    {
        private SpaceEntityData entityData;
        private int amount = 4;


        public SpawnOnDeath(SpaceEntity spaceEntity, SpaceEntityData entityData, int amount) : base(spaceEntity)
        {
            this.entityData = entityData;
            this.amount = amount;
        }


        public override void OnHit()
        {
            if (entityData)
                SpawnPieces();
        }



        private void SpawnPieces()
        {
            var direction = entity.forward;

            for (int i = 0; i < amount; i++)
            {
                entity.entityInteractions.AddToGame(entityData, entity.position, direction * entityData.startSpeed);
                direction.Rotate(360f / amount);
            }
        }
    }
}