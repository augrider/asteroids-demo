using System;
using System.Collections;
using System.Collections.Generic;
using GameScriptableObjects;
using UnityEngine;
using WeaponSystem;

namespace SpaceEntitySystem
{
    public class PlayerInteractions : BaseEntityInteractions
    {
        private float maxAcceleration;
        private float rotationSpeed;

        private WeaponData bullet;
        private WeaponData laser;

        private PlayerControls controls;


        public PlayerInteractions(SpaceEntity spaceEntity, PlayerControls controls, float maxAcceleration, float rotationSpeed) : base(spaceEntity)
        {
            this.controls = controls;

            this.maxAcceleration = maxAcceleration;
            this.rotationSpeed = rotationSpeed;
        }

        public void SetWeapons(WeaponData bullet, WeaponData laser)
        {
            this.bullet = bullet;
            this.laser = laser;
        }


        public override void OnUpdate(bool allowControl)
        {
            if (!allowControl)
                return;

            ControlMovement();
            FireWeapons(entity.entityLauncher);
        }

        public override void OnHit()
        {
            Debug.Log("Player hit!");
        }



        private void ControlMovement()
        {
            entity.physics.SetRotationSpeed(rotationSpeed * Math.Sign(controls.movementInput.x));
            entity.physics.SetAcceleration(entity.forward * maxAcceleration * Math.Sign(controls.movementInput.y));
        }

        private void FireWeapons(Launcher entityLauncher)
        {
            if (controls.shootLaser)
            {
                entityLauncher.FireWeapon(laser);
                return;
            }

            if (controls.shootBullet)
                entityLauncher.FireWeapon(bullet);
        }
    }
}