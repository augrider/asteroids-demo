using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceEntitySystem
{
    public interface IPositionAdapter
    {
        Vector2 position { get; set; }
    }
}