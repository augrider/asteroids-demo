using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceEntitySystem
{
    public interface IBoundaryHandler
    {
        bool AllowedToInvert(bool invertHorizontal, bool invertVertical);
    }
}