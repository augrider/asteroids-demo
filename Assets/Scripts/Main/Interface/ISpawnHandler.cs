using System.Collections;
using System.Collections.Generic;
using GameScriptableObjects.Events;
using UnityEngine;

namespace SpaceEntitySystem.Spawn
{
    public interface ISpawnHandler
    {
        /// <summary>
        /// Spawn entities on field
        /// </summary>
        void SpawnEntities(CreateEntityEvent createEntityEvent, params GameScriptableObjects.SpaceEntityData[] entities);
    }
}