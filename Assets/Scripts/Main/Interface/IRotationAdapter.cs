using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceEntitySystem
{
    public interface IRotationAdapter
    {
        Vector2 forward { get; }
        float angle { get; set; }

        void SetDirection(Vector2 value);
    }
}