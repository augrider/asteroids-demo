using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceEntitySystem
{
    public interface IEntityInteractions
    {
        void OnUpdate(bool allowControl);
        void OnHit();

        /// <summary>
        /// Operations invoked when two entities are concidered collided
        /// </summary>
        /// <returns>Whether or not count collision as hit</returns>
        bool OnCollision(SpaceEntity other);
    }
}