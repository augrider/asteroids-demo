using System.Collections;
using System.Collections.Generic;
using GameScriptableObjects;
using GameScriptableObjects.Events;
using SpaceEntitySystem.Spawn;
using UnityEngine;

namespace ManagerSystem
{
    public class GameArea : MonoBehaviour
    {
        public RectTransform floor => _floor;
        [SerializeField] private RectTransform _floor;

        [SerializeField] private CreateEntityEvent createEntityEvent;
        [SerializeField] private EntityRuntimeSet entities;

        private ISpawnHandler spawnHandler;


        void Awake()
        {
            entities.Reset();
        }


        public void Initialize(ISpawnHandler spawnHandler) => this.spawnHandler = spawnHandler;

        public bool AllowedToSpawnNewWave()
        {
            return entities.count <= 1;
        }

        public void SpawnEntityInCenter(GameScriptableObjects.SpaceEntityData entityData) => createEntityEvent.Raise(entityData, Vector3.zero, Vector2.zero);
        public void SpawnEntities(params GameScriptableObjects.SpaceEntityData[] entities) => spawnHandler.SpawnEntities(createEntityEvent, entities);
    }
}