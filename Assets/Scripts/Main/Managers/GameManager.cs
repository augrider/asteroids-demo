using System;
using System.Collections;
using System.Collections.Generic;
using GameScriptableObjects;
using GameScriptableObjects.Events;
using ScriptableObjectsSystem.Events;
using SpaceEntitySystem;
using SpaceEntitySystem.Spawn;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace ManagerSystem
{
    /// <summary>
    /// Main class for handling gameplay loop
    /// </summary>
    public class GameManager : Singleton<GameManager>
    {
        [SerializeField] private GameSettings gameSettings;
        [SerializeField] private GameState gameState;

        [SerializeField] private PlayerEntityData playerEntityData;

        [SerializeField] private GameArea gameArea;

        [SerializeField] private float onStartGameWait;
        [SerializeField] private float onStartWaveWait;

        private WaitForSeconds waitForWaveStart;


        // Start is called before the first frame update
        IEnumerator Start()
        {
            gameState.Reset();

            waitForWaveStart = new WaitForSeconds(onStartWaveWait);
            gameArea.Initialize(gameSettings.GetSpawnRules(gameArea));

            yield return GameStart();

            StartCoroutine(NewWave());
        }


        public void OnEntityDeath()
        {
            if (gameArea.AllowedToSpawnNewWave())
                StartCoroutine(NewWave());
        }



        private IEnumerator GameStart()
        {
            gameArea.SpawnEntityInCenter(playerEntityData);
            yield return new WaitForSeconds(onStartGameWait);
        }

        private IEnumerator NewWave()
        {
            gameState.AddWave();
            yield return waitForWaveStart;

            Debug.Log("New wave!");
            gameArea.SpawnEntities(gameSettings.GetWaveEntities(gameState.waveCount));
        }
    }
}