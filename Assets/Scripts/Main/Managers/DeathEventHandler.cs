using System.Collections;
using System.Collections.Generic;
using GameScriptableObjects;
using GameScriptableObjects.Events;
using SpaceEntitySystem;
using UnityEngine;

namespace ManagerSystem
{
    public class DeathEventHandler : MonoBehaviour
    {
        [SerializeField] private GameState gameState;
        [SerializeField] private GameSettings gameSettings;

        [SerializeField] private RemoveEntityEvent removeEntityEvent;


        void OnEnable()
        {
            removeEntityEvent.Subscribe(OnEntityDeath);
        }

        void OnDisable()
        {
            removeEntityEvent.Unsubscribe(OnEntityDeath);
        }



        private void OnEntityDeath(SpaceEntity entity)
        {
            gameState.AddScore(entity.entityData.scoreCost);

            if (entity.entityData.isPlayer)
                OnPlayerDeath();
        }

        private void OnPlayerDeath()
        {
            gameSettings.SaveHighScore(gameState.scoreCount, gameState.waveCount);
            GameSceneLoader.current.OnGameOver();
        }
    }
}